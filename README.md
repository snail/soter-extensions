﻿#安装指南
1.下载拓展包zip文件，然后解压，比如解压后是：/home/soter/cron  
2.复制文件夹cron到soter项目的application/packages/里面  
3.启用cron，在入口文件里面修改addPackages，增加extensions  
比如下面：  
```php
/* 注册拓展包 */  
->addPackages(array(  
	....  
	SOTER_PACKAGES_PATH . 'cron',  
	...  
))  
```
